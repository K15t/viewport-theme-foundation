# Viewport theme with Foundation Framework

## Installation

Please make sure to do the following steps:

###General

Make sure you have npm installed

Install Bower

```bash
npm install -g bower 
```

After that install the dependencies for Foundation for sites

```bash
bower install
```

install the dependencies for viewport

```bash
npm install
```

###Gulpfile.js

Include your viewport-URL of your space here for browser-sync
```bash
var BROWSERSYNC_URL = 'http://localhost:1990/confluence/test';
```

Make sure your theme name is entered here. The name should be the same like in Confluence.
```bash
var THEME_NAME = 'example';
```

If the theme is not created in Confluence yet, you can set it up by typing
```bash
gulp create
```
into your console.


## Developing Documentations

[Documentation of the Foundation Framework](https://foundation.zurb.com/sites/docs/)

[Documentation of Scroll Viewport](https://help.k15t.com/scroll-viewport/latest/scroll-viewport-documentation-125708693.html#manual)

[Documentation of Apache Velocity Template](http://velocity.apache.org/engine/1.7/user-guide.html)
