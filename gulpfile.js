// Example gulpfile.js
// install all dependencies first (run npm install)
//
// Configuration:
//   TARGET -- the target to deploy to
//
// Tasks:
//   upload -- full build & upload
//   reset-theme -- remove all files from theme
//   watch -- watch (to be used during development)

var browserSync     = require('browser-sync').create();
var gulp            = require('gulp');
var clean           = require('gulp-clean');
var ViewportTheme   = require('gulp-viewport');
var $               = require('gulp-load-plugins')();
var concat          = require('gulp-concat');

// The target system needs to match with a section in .viewportrc
// How to use the different environments within the .viewportrc file is explained here: https://github.com/K15t/gulp-viewport#get-started
var TARGET = 'DEV';

// The scope (space) of the theme. Specify space key to install
// the theme into a space (empty string the theme will be global).
var SCOPE = '';

var THEME_NAME = 'your-theme-name';

// Enter Viewport URL here to enable browser sync, e.g.
// https://localhost:1990/confluence/test
var BROWSERSYNC_URL = '';



var viewportTheme = new ViewportTheme({
    env: TARGET,
    scope: SCOPE,
    themeName: THEME_NAME,
    sourceBase: 'src'
});



// Creates the Viewport Theme with name found in THEME_NAME in Confluence
gulp.task('create', function() {
    if(!viewportTheme.exists()) {
        viewportTheme.create();
    } else {
        console.log('Theme with name \'' + THEME_NAME + '\' already exists.');
    }
});



// Uploads all files
gulp.task('upload', ['reset-theme', 'fonts', 'img', 'js', 'sass', 'templates']);



// Uploads all files, watch changes on files and reloads automatically browser window
gulp.task('watch',['upload'], function () {
    if (BROWSERSYNC_URL !== '') {
        browserSync.init({
            proxy: BROWSERSYNC_URL
        });
        viewportTheme.on('uploaded', browserSync.reload);
    }

    viewportTheme.on('uploaded', browserSync.reload);
    gulp.watch('src/fonts/**/*', ['fonts']);
    gulp.watch('src/img/**/*', ['img']);
    gulp.watch('src/js/**/*', ['js']);
    gulp.watch('src/scss/**/*.scss', ['sass']);
    gulp.watch('src/**/*.vm', ['templates']);
});



gulp.task('fonts', function () {
    return gulp.src('src/fonts/**/*.*')
        .pipe(viewportTheme.upload())
        .pipe(gulp.dest('build/fonts'));
});



gulp.task('img', function () {
    return gulp.src('src/img/**/*')
        .pipe(viewportTheme.upload())
        .pipe(gulp.dest('build/img'));
});



gulp.task('js', function () {
    return gulp.src([
        './bower_components/jquery/dist/jquery.js',
        './bower_components/what-input/dist/what-input.js',
        './bower_components/foundation-sites/dist/js/foundation.js',
        //Insert additional external .js files here
        'src/js/**/*.*'])
        .pipe(concat('build/js/app.js'))
        .pipe(gulp.dest(''))
        .pipe(viewportTheme.upload({
            sourceBase: 'build/js/app.js',
            targetPath: 'js/app.js'
        }))
});



// Paths for sass-files of ZURB Foundation
var sassPaths = [
    './bower_components/normalize.scss/sass',
    './bower_components/foundation-sites/scss',
    './bower_components/motion-ui/src'
];

// sass compile & concat into one file
gulp.task('sass', function() {
    return gulp.src('src/scss/app.scss')
        .pipe($.sass({
            includePaths: sassPaths,
            outputStyle: 'compressed' // if css compressed **file size**
        })
            .on('error', $.sass.logError))
        .pipe($.autoprefixer({
            browsers: ['last 2 versions', 'ie >= 9']
        }))
        .pipe(gulp.dest('build/css'))
        .pipe(viewportTheme.upload(
            {
                sourceBase: 'build/css/app.css',
                targetPath: 'css/app.css'
            }
        ))
});



gulp.task('templates', function () {
    return gulp.src('src/**/*.vm')
        .pipe(viewportTheme.upload())
        .pipe(gulp.dest('build'));
});



// Reset your theme (empties the Theme directory in Confluence)
gulp.task('reset-theme', function () {
    viewportTheme.removeAllResources();
    return gulp.src('build', {read: false})
        .pipe(clean());
});
